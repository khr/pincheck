// pincheck header file to be included into user code

// in case mpi.h has been included previously, we can enable MPI within pincheck
#ifndef PINCHK_USE_MPI
    #ifdef MPI_VERSION
        #define PINCHK_USE_MPI
    #endif
#endif

// in case the compiler is run with OpenMP enabled, we can enable OMP within pincheck
#ifndef PINCHK_USE_OMP
    #ifdef _OPENMP
        #define PINCHK_USE_OMP
    #endif
#endif

#include <pincheck_implementation.hpp>

